#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski


class EventBase:

    def __init__(self, eventId):
        self._eventId = eventId

    @property
    def eventId(self):
        return self._eventId
