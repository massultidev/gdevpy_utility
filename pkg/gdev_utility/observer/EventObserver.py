#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

import weakref
from enum import Enum
from threading import RLock
from gdev_utility.Loggable import Loggable
from gdev_utility.observer.Subscription import Subscription


class EventObserver(Loggable):

    def __init__(self, eventEnum):
        if not issubclass(eventEnum, Enum):
            raise TypeError('EventObserver only supports enum based events!')

        self._lock = RLock()
        self._eventEnum = eventEnum
        self._subscriptionRefCache = {}
        self._subscriptions = {}

    def subscribeEvent(self, eventId, callback):
        with self._lock:
            return self._subscribeEvents([eventId], callback)

    def subscribeEvents(self, eventIds, callback):
        with self._lock:
            return self._subscribeEvents(eventIds, callback)

    def _subscribeEvents(self, eventIds, callback):
        for eventId in eventIds:
            self._verifyEventId(eventId)

        subscription = Subscription(callback)
        subscriptionRef = weakref.ref(subscription, self._collectSubscriptionRef)

        self._subscriptionRefCache[subscriptionRef] = eventIds

        for eventId in eventIds:
            self._addSubscription(eventId, subscriptionRef)

        return subscription

    def _addSubscription(self, eventId, subscriptionRef):
        if eventId not in self._subscriptions.keys():
            self._subscriptions[eventId] = []
        self._subscriptions[eventId].append(subscriptionRef)

    def _verifyEventId(self, eventId):
        if not isinstance(eventId, self._eventEnum):
            raise TypeError('Unknown event ID!')

    def notify(self, event):
        with self._lock:
            eventId = event.eventId

            self._verifyEventId(eventId)

            if eventId not in self._subscriptions.keys():
                self.log.debug('No one to notify.')
                return

            for subscriptionRef in self._subscriptions[eventId]:
                subscription = subscriptionRef()
                if subscription is None:
                    continue
                subscription.callback(event)

    def _collectSubscriptionRef(self, reference):
        with self._lock:
            eventIds = self._subscriptionRefCache[reference]

            for eventId in eventIds:
                self._subscriptions[eventId].remove(reference)

            self._subscriptionRefCache.pop(reference)
