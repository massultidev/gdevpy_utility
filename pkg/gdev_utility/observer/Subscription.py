#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski


class Subscription:

    def __init__(self, callback):
        self._callback = callback

    @property
    def callback(self):
        return self._callback
