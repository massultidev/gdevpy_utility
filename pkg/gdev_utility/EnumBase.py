#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

import enum


class EnumBase(enum.Enum):
    def __new__(cls):
        value = len(getattr(cls, "__members__")) + 1
        obj = object.__new__(cls)
        obj._value_ = value
        return obj

    def __str__(self):
        return self.name.replace("_", ".").lower()

    @classmethod
    def list(cls):
        return [member.replace("_", ".").lower()
                for member in getattr(cls, "__members__")]
