#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

import inspect


getCurrentMethodName = lambda: inspect.stack()[1][3]
