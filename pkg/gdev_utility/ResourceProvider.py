#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright by: P.J. Grochowski

import os


class ResourceProvider:

    def __init__(self, package):
        self.dir = os.path.join(os.path.dirname(package.__file__), '_resource')

    def getResourcePath(self, name):
        return os.path.join(self.dir, name)
